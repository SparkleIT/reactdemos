import React from 'react';
import {Image, View, StyleSheet, Text, Component} from 'react-native';
import BalanceLabel from './BalanceLabel.js'

class Balance extends React.Component {
    render() {
        return (
            <View style={this.props.style}>
                <View style={styles.mainContainerStyle}>
                    <View style={styles.containerStyle}>
                        <Image source ={require('../images/wallet.png')} style={styles.iconStyle}/>
                        <Text style={styles.titleStyle}>
                            Your Wallet Balance
                        </Text>
                    </View>
                    <BalanceLabel/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    mainContainerStyle: {
        padding: 5,
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#FFFFFF',
        borderRadius: 5
    },

    containerStyle: {
        flexDirection: 'row'
    },

    iconStyle: {
        resizeMode: Image.resizeMode.center
    },

    titleStyle: {
        alignSelf: 'center',
        fontSize: 14,
        color: '#525979',
        fontFamily: 'Open Sans',
        fontWeight: 'bold'
    }
})

export default Balance;
