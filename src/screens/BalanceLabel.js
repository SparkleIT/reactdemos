import React from 'react';
import {View, StyleSheet, Text, Component} from 'react-native';

class BalanceLabel extends React.Component {
    render() {
        return (
            <View style={styles.containerStyle}>
                <Text style={styles.titleStyle}>
                    $150.0
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    containerStyle: {
        padding: 7,
        paddingLeft: 12,
        paddingRight: 12,
        backgroundColor: '#78c6d8',
        borderRadius: 20,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    titleStyle: {
        fontSize: 15,
        color: "#FFFFFF",
        fontWeight: 'bold',
        fontFamily: 'Open Sans'
    }
})

export default BalanceLabel;
