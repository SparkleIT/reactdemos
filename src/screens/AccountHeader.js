import React, {Component} from 'react';
import {View, Text, StyleSheet, ImageBackground, Image} from 'react-native';
import Balance from './Balance.js'

class AccountHeader extends React.Component {
    render() {
        return (
            <ImageBackground
                source={require('../images/lawrance.jpg')}
                style={styles.container}>
                <View style={styles.overlay}>
                    <Text
                        style={[
                        styles.boldTextStyle, {
                            paddingTop: 30
                        }]}>My Account</Text>
                    <Image source={require('../images/lawrance.jpg')} style={styles.avatarStyle}/>
                    <Text style={styles.boldTextStyle}>
                        Jenifer Lawrance</Text>
                    <Text style={styles.regularTextStyle}>
                        +14155552671</Text>
                    <Balance style={styles.balanceContainer}/>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {},
    overlay: {
        backgroundColor: 'rgba(96, 76, 151,0.5)'
    },
    avatarStyle: {
        width: 100,
        height: 100,
        marginTop: 10,
        borderRadius: 50,
        alignSelf: 'center'
    },
    boldTextStyle: {
        marginTop: 10,
        fontSize: 18,
        color: "#FFFFFF",
        fontFamily: 'Open Sans',
        fontWeight: 'bold',
        alignSelf: 'center'
    },

    regularTextStyle: {
        marginTop: 10,
        fontSize: 18,
        color: "#FFFFFF",
        fontFamily: 'Open Sans',
        alignSelf: 'center'
    },
    balanceContainer: {
        padding: 10
    }
});

export default AccountHeader;