import React, {Components} from 'react';
import {View, StyleSheet, Image} from 'react-native';
import AccountHeader from './AccountHeader';
import MyAccountOptions from './MyAccountOptions';

export default class MyAccount extends React.Component{
    render()
    {
      return (
        <View > 
          <AccountHeader/> 
          <MyAccountOptions style= {styles.containerStyle}/>
        </View>
      );
    }
}

const styles = StyleSheet.create({
  containerStyle: {
    marginLeft: 10,
    marginRight:10
  },
})