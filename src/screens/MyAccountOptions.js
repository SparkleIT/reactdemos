import React from 'react';
import {Component, View, Text, Image, FlatList, StyleSheet} from 'react-native';
import {List, ListItem} from 'react-native-elements'

var screenData = [
    {
      id: "1",
      image: require('../images/saved_cards.png'),
      title:"Saved Cards",
    },
    {
      id: "2",
      image: require('../images/bank_account.png'),
      title:"Bank Accounts",
    },
    {
      id: "3",
      image: require('../images/qr_code.png'),
      title:"QR Code",
    },
    {
      id:"4",
      image: require('../images/touch_id.png'),
      title:"Touch ID",
    },
    {
      id:"5",
      image: require('../images/language.png'),
      title:"Select Language",
    },
    {
      id: "6",
      image: require('../images/help_icon.png'),
      title:"Help",
    },
  ];

  renderSeparator = () => {
    return (
      <View
        style={ styles.separatorStyle}
      />
    );
  };

  
class MyAccountOptions extends React.Component{
    render(){
        return(
          <View style= {this.props.style}>
            <List containerStyle={styles.containerStyle}>
                <FlatList
                    data={screenData}
                    renderItem= {({item}) =>
                        <ListItem
                            leftIcon = {<Image source={item.image} style= {styles.leftIcon}/>}
                            title= {item.title}
                            titleContainerStyle ={item.titleContainerStyle}
                            titleStyle= {styles.titleStyle}
                            fontFamily= "Entypo"/>}
                    keyExtractor={item => item.id}
                    ItemSeparatorComponent={this.renderSeparator}  />
        </List>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    containerStyle: {
      borderTopWidth: 0, 
      borderBottomWidth: 0,
      borderRadius: 5
    },
    titleStyle: {
      fontSize: 14,
      justifyContent: 'center',
    },
    titleContainerStyle: {
      marginLeft: 10,
    },
    leftIcon: {
      resizeMode: Image.resizeMode.center,
    },
    separatorStyle:{
        height: 2,
        width: "86%",
        backgroundColor: "#525979",
        marginLeft: "14%",
    }
  })

  export default MyAccountOptions;
  