import React from 'react';
import {Button, View, Text, FlatList} from 'react-native';
import {StackNavigator} from 'react-navigation'; // Version can be specified in package.json
import MyAccount from './MyAccount';

const customNavigationHeader = {
  style: {
    'headerTitle': <CustomHeader/>,
    'headerRight': (<Button onPress= {()=> alert('THis is alert!')} title='Info' color='#fff'/>),
    'headerBackTitle': null
  }
}

const navigationHeaderStyle = {
  style: {
    'title': "Home",
    'headerStyle': {
      backgroundColor: '#525979'
    },
    'headerTintColor': '#fff',
    'headerTitleStyle': {
      'fontWeight': 'bold'
    },
    'headerBackTitle': null
  }
};

const detailsNavigationHeaderStyle = {
  style: {
    'title': "Details",
    'headerStyle': {
      backgroundColor: '#000'
    },
    'headerTintColor': '#fff',
    'headerTitleStyle': {
      'fontWeight': 'bold'
    }
  }
};

class HomeScreen extends React.Component {
  render() {
    return (
      <View
        style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
        <Text>Home Screen</Text>
        <Button
          title="Go to Details"
          onPress={() => {
          this
            .props
            .navigation
            .navigate('MyModal');
        }}/>
      </View>
    );
  }
}

class DetailsScreen extends React.Component {
  render() {
    return (
      <View
        style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
        <Text>Details Screen</Text>
        <Button
          title="Go to Details... again"
          onPress={() => this.props.navigation.navigate('Details')}/>
        <Button title="Go back" onPress={() => this.props.navigation.goBack()}/>
      </View>
    );
  }
}

class ModalScreen extends React.Component {
  render() {
    return (
      <View
        style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
      }}>
        <Text>This is ModalScreen!</Text>
        <Button title="Dismiss" onPress={() => this.props.navigation.goBack()}/>
      </View>
    );
  }
}

class CustomHeader extends React.Component {
  render() {
    return (
      <Text style={{
        flex: 1,
        backgroundColor: '#fff'
      }}>
        CustomHeader
      </Text>
    );
  }
}

const MainStack = StackNavigator({
  MyAccount: {
    screen: MyAccount
  },
  Home: {
    screen: HomeScreen
  },
  Details: {
    screen: DetailsScreen
  }
}, {
  mode: 'modal',
  headerMode: 'none',
  // navigationOptions: navigationHeaderStyle.style,
});

const RootStack = StackNavigator({
  Main: {
    screen: MainStack
  },
  MyModal: {
    screen: ModalScreen
  }
}, {
  mode: 'modal',
  headerMode: 'none'
});

export default RootStack;
