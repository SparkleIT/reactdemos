import React from 'react';
import { StyleSheet, Text, View, AppRegistry, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';

export default class App extends React.Component<Props> {
  static navigationOptions = {
    title: 'Welcome',
  };
  render() {
    return (
      <View>
        <Text style={style.mainfont}>Hello, Chat App!</Text>
        <Button
          onPress={() => navigate('Chat')}
          title="Chat with Lucy"
        />
      </View>
    );
  }
}

class ChatScreen extends React.Component {
  static navigationOptions = {
    title: 'Chat with Lucy',
  };
  render() {
    return (
      <View>
        <Text>Chat with Lucy</Text>
      </View>
    );
  }
}

const SimpleApp = StackNavigator({
  Home: { screen: App },
  Chat: { screen: ChatScreen },
});

export default SimpleApp;

const style = StyleSheet.create({
  mainfont:{
    flex: 1,
    fontSize: 20,
    margin:40
  },
  container:{
    background-color: #000000
  }
})
